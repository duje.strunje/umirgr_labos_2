extends Node2D

var SPEED_MAX = 2000
var SPEED_MIN = 600
var paddle_hit_counter = 0;
const paddle_hit_step = 4
var points_first = 0;
var points_second = 0;

const PADDLE_SPEED = 16


# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("pong").velocity = Vector2(SPEED_MIN, 000)

func _process(delta):
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()

var pong_direction = 1;

func reset_table(side, ball):
	SPEED_MIN = 600
	SPEED_MAX = 1200
	paddle_hit_counter = 0;
	get_node("pong").set_position(Vector2(500, 300))
	get_node("pong/Line2D").reset_queue()
	
	if (side == "border"):
		points_second +=1
		get_node("player2score").text = str(points_second)
	elif (side == "border2"):
		points_first +=1
		get_node("player1score").text = str(points_first)
	
	var new_text = ""
	if (points_first == 10):
		new_text = "Pobijedio prvi"
	elif (points_second == 10):
		new_text = "Pobijedio drugi"
	get_node("info").text = new_text
	if (new_text):
		await get_tree().create_timer(5.0).timeout 
		points_first = 0
		points_second = 0;
		get_node("player2score").text = str(points_second)
		get_node("player1score").text = str(points_first)
		get_node("info").text = ""
		ball.velocity = Vector2(SPEED_MIN, 0)
	
func _physics_process(delta):
	var ball = get_node("pong")
	
	var rd = Input.get_axis("r_up", "r_down")
	if rd == 1:
		get_node("player2").position.y += PADDLE_SPEED
	elif rd == -1:
		get_node("player2").position.y -= PADDLE_SPEED
	
	var ld = Input.get_axis("l_up", "l_down")
	if ld == 1:
		get_node("player1").position.y += PADDLE_SPEED
	elif ld == -1:
		get_node("player1").position.y -= PADDLE_SPEED
	
	var collision_info = ball.move_and_collide(ball.velocity * delta)
	if collision_info:
		if collision_info.get_collider().get_name() == "border":
			reset_table(collision_info.get_collider().get_parent().get_name(), ball)
			if (points_first == 10 or points_second == 10):
				ball.velocity = Vector2(0, 0)
			else:
				ball.velocity = Vector2(SPEED_MIN, 0)
			return
		
		ball.velocity = ball.velocity.bounce(collision_info.get_normal())
		
		if (collision_info.get_collider().get_name() == "CharacterBody2D"):
			var velocity = ball.velocity.length()
			var dir = ball.velocity.normalized()
			var way = collision_info.get_normal().x
			var index = collision_info.get_collider_shape_index()
			
			if index == 0:
				if velocity > SPEED_MIN:
					velocity -= 100
				var new_angle = ball.velocity.angle_to(collision_info.get_normal())
				dir = ball.velocity.normalized().rotated(new_angle/2 * way)
			else:
				if velocity < SPEED_MAX:
					velocity += 100
				if ball.velocity.angle_to(collision_info.get_normal()) < deg_to_rad(75):
					if index == 1:
						dir = dir.rotated(-0.45 * way)
					else: 
						dir = dir.rotated(0.45 * way)
				
			paddle_hit_counter += 1
			if paddle_hit_counter > 0 and paddle_hit_counter % paddle_hit_step == 0:
				SPEED_MIN += 100
				SPEED_MAX += 100
				velocity += 100
			ball.velocity = dir * velocity
